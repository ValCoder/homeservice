﻿namespace HouseServicePlatform.Models.DataModels
{
    public class OffersCustomerSupplier
    {
        public int OfferId { get; set; }

        public Offer Offer { get; set; }

        public string CustomerId { get; set; }

        public User Customer { get; set; }

        public string SupplierId { get; set; }

        public User Supplier { get; set; }

        public int JobId { get; set; }

        public JobService Job { get; set; }
        
    }
}
