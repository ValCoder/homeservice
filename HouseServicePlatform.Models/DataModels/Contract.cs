﻿namespace HouseServicePlatform.Models.DataModels
{

    public class Contract
    {
        public int Id { get; set; }

        public string CotractName { get; set; }

        public decimal TotalPricePerDay { get; set; }

        public decimal PricePerHour { get; set; }

        public int TotalHours { get; set; }

        public ContractsCustomerSupplier ContractsCustomerSupplier { get; set; }

        public bool IsSignedByCustomer { get; set; }

        public int JobId { get; set; }

        public JobService Job { get; set; }

    }
}
