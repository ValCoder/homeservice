﻿namespace HouseServicePlatform.Models.DataModels
{
    public class Review
    {
        public int Id { get; set; }

        public string AuthordId { get; set; }

        public User Author { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
    }
}
