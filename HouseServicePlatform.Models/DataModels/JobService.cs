﻿namespace HouseServicePlatform.Models.DataModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Enums.SupplierCategories;

    public class JobService
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public Category FieldOfWork { get; set; }

        public DateTime? JobStartDate { get; set; }

        public DateTime? JobEndDate { get; set; }

        public bool IsJobTaken { get; set; }

        public string CustomerId { get; set; }

        public User Customer { get; set; }

        public Location Location { get; set; }

        public int LocationId { get; set; }

    }
}
