﻿namespace HouseServicePlatform.Models.DataModels
{
    using System.Collections.Generic;
    using Enums;
    using Enums.SupplierCategories;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNetCore.Identity;

    public class User : IdentityUser
    {
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public bool IsSupplier { get; set; }

        public Category FiledOfWork { get; set; }

        public decimal PricePerHour { get; set; }

        public int MaxWeekHourWork { get; set; }

        public Week WeekStarts { get; set; }

        [Range(0, 5)]
        public int Rating { get; set; }

        public ICollection<Location> Locations { get; set; } = new List<Location>();

        public ICollection<JobService> JobServices { get; set; } = new List<JobService>();

        public ICollection<Review> Reviews { get; set; } = new List<Review>();

        public ICollection<OffersCustomerSupplier> OffersCustomersSupplier { get; set; } = new List<OffersCustomerSupplier>();

        public ICollection<ContractsCustomerSupplier> ContractsCustomersSupplier { get; set; } = new List<ContractsCustomerSupplier>();

    }
}
