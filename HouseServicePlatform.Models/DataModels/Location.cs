﻿namespace HouseServicePlatform.Models.DataModels
{
    public class Location
    {
        public int Id { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }

        public int StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string NeighborhoodName { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public User User { get; set; }

        public string UserId { get; set; }

    }
}
