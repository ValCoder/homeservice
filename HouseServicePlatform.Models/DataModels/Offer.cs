﻿namespace HouseServicePlatform.Models.DataModels
{
    public class Offer
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public bool IsSent { get; set; }

        public bool SupplierHasSentContract { get; set; }

        public bool CustomerHasRequestedContract { get; set; }

        public OffersCustomerSupplier OfferCustomerSupplier { get; set; }

    }
}
