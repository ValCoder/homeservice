﻿namespace HouseServicePlatform.Models.DataModels
{
    public class ContractsCustomerSupplier
    {
        public Contract Contract { get; set; }

        public int ContractId { get; set; }

        public User Customer { get; set; }

        public string CustomerId { get; set; }

        public User Supplier { get; set; }

        public string SupplierId { get; set; }

    }
}
