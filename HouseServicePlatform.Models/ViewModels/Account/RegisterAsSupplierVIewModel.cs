﻿namespace HouseServicePlatform.Models.ViewModels.Account
{
    using Enums;
    using Enums.SupplierCategories;
    using System.ComponentModel.DataAnnotations;

    public class RegisterAsSupplierViewModel : RegisterAsCustomerViewModel
    {
        [Display(Name = "Your filed of work")]
        public Category FiledOfWork { get; set; }

        [Display(Name = "Price per hour")]
        public decimal PricePerHour { get; set; }

        [Display(Name = "Maximum working hours in week")]
        [Range(minimum: 10, maximum: 40)]
        public int MaxWeekHourWork { get; set; }

        [Display(Name = "Working week start")]
        public Week WeekStarts { get; set; }

    }
}
