﻿namespace HouseServicePlatform.Models.ViewModels.Supplier.Jobs
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;
    using System.ComponentModel.DataAnnotations;
    using System;

    public class JobDetailsVm : IMapFrom<JobService>, IHaveCustomMapping
    {
        public int Id { get; set; }

        public string Title { get; set; }

        [Display(Name = "Job Description")]
        public string Description { get; set; }

        [Display(Name = "Field Of Work")]
        public Category FieldOfWork { get; set; }

        [Display(Name = "Approximately working hours")]
        public int RoughlyWorkHours { get; set; }

        [Display(Name = "Client First Name")]
        public string CustomerFirstName { get; set; }

        [Display(Name = "Phone Number")]
        public string CustomerPhoneNumber { get; set; }

        [Display(Name = "Client Last Name")]
        public string CustomerLastName { get; set; }

        [Display(Name = "Street Number")]
        public int StreetNumber { get; set; }

        [Display(Name = "Street Name")]
        public string StreetName { get; set; }

        [Display(Name = "Neighborhood Name")]
        public string NeighborhoodName { get; set; }

        [Display(Name = "Email")]
        public string CustomerEmail { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            #region mappings

            string customerId = String.Empty;

            mapper.CreateMap<JobService, JobDetailsVm>()
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(j => j.Customer.FirstName))
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(j => j.Customer.LastName))
                .ForMember(vm => vm.StreetName, cfg => cfg.MapFrom(j => j.Location.StreetName))
                .ForMember(vm => vm.NeighborhoodName, cfg => cfg.MapFrom(j => j.Location.NeighborhoodName))
                .ForMember(vm => vm.StreetNumber, cfg => cfg.MapFrom(j => j.Location.StreetNumber))
                .ForMember(vm => vm.City, cfg => cfg.MapFrom(j => j.Location.City))
                .ForMember(vm => vm.Country, cfg => cfg.MapFrom(j => j.Location.Country))
                .ForMember(vm => vm.PostalCode, cfg => cfg.MapFrom(j => j.Location.PostalCode))
                .ForMember(vm => vm.CustomerPhoneNumber, cfg => cfg.MapFrom(j => j.Customer.PhoneNumber))
                .ForMember(vm => vm.CustomerEmail, cfg => cfg.MapFrom(j => j.Customer.Email));

            #endregion

        }
    }
}
