﻿namespace HouseServicePlatform.Models.ViewModels.Supplier.Jobs
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;


    public class AllJobsVmSupplier : IMapFrom<JobService>, IHaveCustomMapping
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Category FieldOfWork { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string City { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<JobService, AllJobsVmSupplier>()
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(j => j.Customer.FirstName));

            mapper.CreateMap<JobService, AllJobsVmSupplier>()
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(j => j.Customer.LastName));

            mapper.CreateMap<JobService, AllJobsVmSupplier>()
                .ForMember(vm => vm.City, cfg => cfg.MapFrom(j => j.Location.City));
        }
    }
}
