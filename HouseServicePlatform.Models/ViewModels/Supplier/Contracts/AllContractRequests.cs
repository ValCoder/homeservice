﻿namespace HouseServicePlatform.Models.ViewModels.Supplier.Contracts
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;

    public class AllContractRequests : IHaveCustomMapping
    {
        public int OfferId { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public Category JobCategory { get; set; }

        public string JobTitle { get; set; }

        public bool SupplierHasSentContract { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, AllContractRequests>()
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(ocs => ocs.Customer.FirstName))
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(ocs => ocs.Customer.LastName))
                .ForMember(vm => vm.JobCategory, cfg => cfg.MapFrom(ocs => ocs.Job.FieldOfWork))
                .ForMember(vm => vm.JobTitle, cfg => cfg.MapFrom(ocs => ocs.Job.Title))
                .ForMember(vm => vm.OfferId, cfg => cfg.MapFrom(ocs => ocs.OfferId))
                .ForMember(vm => vm.SupplierHasSentContract, cfg => cfg.MapFrom(ocs => ocs.Offer.SupplierHasSentContract));
            
        }
    }
}
