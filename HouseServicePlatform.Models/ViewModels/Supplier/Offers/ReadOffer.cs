﻿namespace HouseServicePlatform.Models.ViewModels.Supplier.Offers
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;

    public class ReadOffer : IHaveCustomMapping
    {
        public string OfferTitle { get; set; }

        public string OfferContent { get; set; }

        public Category FieldOfWork { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string CustomerLastName { get; set; }

        public int StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string NeighborhoodName { get; set; }

        public string CustomerEmail { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            #region mappings

            mapper.CreateMap<OffersCustomerSupplier, ReadOffer>()
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(ocs => ocs.Customer.FirstName))
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(ocs => ocs.Customer.LastName))
                .ForMember(vm => vm.StreetName, cfg => cfg.MapFrom(ocs => ocs.Job.Location.StreetName))
                .ForMember(vm => vm.NeighborhoodName, cfg => cfg.MapFrom(ocs => ocs.Job.Location.NeighborhoodName))
                .ForMember(vm => vm.StreetNumber, cfg => cfg.MapFrom(ocs => ocs.Job.Location.StreetNumber))
                .ForMember(vm => vm.City, cfg => cfg.MapFrom(ocs => ocs.Job.Location.City))
                .ForMember(vm => vm.Country, cfg => cfg.MapFrom(ocs => ocs.Job.Location.Country))
                .ForMember(vm => vm.PostalCode, cfg => cfg.MapFrom(ocs => ocs.Job.Location.PostalCode))
                .ForMember(vm => vm.CustomerPhoneNumber, cfg => cfg.MapFrom(ocs => ocs.Customer.PhoneNumber))
                .ForMember(vm => vm.CustomerEmail, cfg => cfg.MapFrom(ocs => ocs.Customer.Email))
                .ForMember(vm => vm.OfferTitle, cfg => cfg.MapFrom(ocs => ocs.Offer.Title))
                .ForMember(vm => vm.OfferContent, cfg => cfg.MapFrom(ocs => ocs.Offer.Content));

            #endregion

        }
    }
}
