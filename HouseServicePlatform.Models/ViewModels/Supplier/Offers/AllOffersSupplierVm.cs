﻿namespace HouseServicePlatform.Models.ViewModels.Supplier.Offers
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;

    using Enums.SupplierCategories;

    public class AllOffersSupplierVm : IHaveCustomMapping
    {
        public int Id { get; set; }

        public string JobTitle { get; set; }

        public Category JobCategory { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string CustomerEmail { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, AllOffersSupplierVm>()
                .ForMember(vm => vm.Id, cfg => cfg.MapFrom(ocs => ocs.OfferId))
                .ForMember(vm => vm.JobTitle, cfg => cfg.MapFrom(ocs => ocs.Job.Title))
                .ForMember(vm => vm.JobCategory, cfg => cfg.MapFrom(ocs => ocs.Job.FieldOfWork))
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(ocs => ocs.Customer.FirstName))
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(ocs => ocs.Customer.LastName))
                .ForMember(vm => vm.CustomerEmail, cfg => cfg.MapFrom(ocs => ocs.Customer.Email));
        }
    }
}
