﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Jobs
{
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;
    using AutoMapper;

    public class AllJobsVmCustomer : IMapFrom<JobService>
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Category FieldOfWork { get; set; }

        public bool IsJobTaken { get; set; }

        public int OffersCountForJob { get; set; }
        
    }
}
