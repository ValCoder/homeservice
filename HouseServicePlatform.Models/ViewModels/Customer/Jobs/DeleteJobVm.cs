﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Jobs
{

    using Common.Mapping;
    using DataModels;

    public class DeleteJobVm : IMapFrom<JobService>
    {
        public int  Id { get; set; }

        public string Title { get; set; }
    }
}
