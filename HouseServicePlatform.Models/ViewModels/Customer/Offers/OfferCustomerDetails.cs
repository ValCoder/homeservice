﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Offers
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;
    using Enums;

    public class OfferCustomerDetails : IMapFrom<OfferCustomerDetails>, IHaveCustomMapping
    {
        public int OfferId { get; set; }

        public string OfferTittle { get; set; }

        public string OfferContent { get; set; }

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public string SupplierEmail { get; set; }

        public Category SupplierFieldOfWork { get; set; }

        public int SupplierMaxWeekWorkHours { get; set; }

        public string SupplierPhoneNumber { get; set; }

        public string SupplierPricePerHour { get; set; }

        public Week SupplierWeekStart { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, OfferCustomerDetails>()
                .ForMember(vm => vm.SupplierEmail, cfg => cfg.MapFrom(ocs => ocs.Supplier.Email))
                .ForMember(vm => vm.SupplierFirstName, cfg => cfg.MapFrom(ocs => ocs.Supplier.FirstName))
                .ForMember(vm => vm.SupplierLastName, cfg => cfg.MapFrom(ocs => ocs.Supplier.LastName))
                .ForMember(vm => vm.SupplierFieldOfWork, cfg => cfg.MapFrom(ocs => ocs.Supplier.FiledOfWork))
                .ForMember(vm => vm.SupplierMaxWeekWorkHours, cfg => cfg.MapFrom(ocs => ocs.Supplier.MaxWeekHourWork))
                .ForMember(vm => vm.SupplierPhoneNumber, cfg => cfg.MapFrom(ocs => ocs.Supplier.PhoneNumber))
                .ForMember(vm => vm.SupplierPricePerHour, cfg => cfg.MapFrom(ocs => ocs.Supplier.PricePerHour))
                .ForMember(vm => vm.SupplierWeekStart, cfg => cfg.MapFrom(ocs => ocs.Supplier.WeekStarts))
                .ForMember(vm => vm.OfferId, cfg => cfg.MapFrom(ocs => ocs.Offer.Id))
                .ForMember(vm => vm.OfferTittle, cfg => cfg.MapFrom(ocs => ocs.Offer.Title))
                .ForMember(vm => vm.OfferContent, cfg => cfg.MapFrom(ocs => ocs.Offer.Content));
        }
    }
}
