﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Offers
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums;
    using Enums.SupplierCategories;

    public class AllOffersCustomerVm : IMapFrom<OffersCustomerSupplier>, IHaveCustomMapping
    {

        public int OfferId { get; set; }

        public string JobTitle { get; set; }

        public Category JobCategory { get; set; }

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public string SupplierEmail { get; set; }

        public decimal SupperPricePerHour { get; set; }

        public Week SupplierWeekStarts { get; set; }

        public int SupplierMaxWeekHourWork { get; set; }

        public Category SupplierFieldOfWork { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, AllOffersCustomerVm>()
                .ForMember(vm => vm.SupplierFirstName, cfg => cfg.MapFrom(ocs => ocs.Supplier.FirstName))
                .ForMember(vm => vm.SupplierLastName, cfg => cfg.MapFrom(ocs => ocs.Supplier.LastName))
                .ForMember(vm => vm.SupplierEmail, cfg => cfg.MapFrom(ocs => ocs.Supplier.Email))
                .ForMember(vm => vm.SupperPricePerHour, cfg => cfg.MapFrom(ocs => ocs.Supplier.PricePerHour))
               .ForMember(vm => vm.JobTitle, cfg => cfg.MapFrom(ocs => ocs.Job.Title))
               .ForMember(vm => vm.JobCategory, cfg => cfg.MapFrom(ocs => ocs.Job.FieldOfWork))
               .ForMember(vm => vm.OfferId, cfg => cfg.MapFrom(ocs => ocs.Offer.Id));

        }
    }
}
