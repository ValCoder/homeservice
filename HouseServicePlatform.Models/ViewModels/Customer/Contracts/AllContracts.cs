﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Contracts
{
    using Enums.SupplierCategories;

    public class AllContracts
    {
        public int ContractId { get; set; }

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public Category JobCategory { get; set; }

        public string JobTitle { get; set; }

        public bool IsSignedByCustomer { get; set; }
    }
}
