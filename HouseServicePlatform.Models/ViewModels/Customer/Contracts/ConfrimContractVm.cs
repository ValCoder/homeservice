﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Contracts
{
    using System;
    using DataModels;
    using Enums.SupplierCategories;
    using AutoMapper;
    using Common.Mapping;


    public class ConfrimContractVm : IHaveCustomMapping
    {
        public int ContracId { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public string SupplierEmail { get; set; }

        public string SupplierPhoneNumber { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string CustomerEmail { get; set; }

        public Category JobCategory { get; set; }

        public string JobTitle { get; set; }

        public DateTime JobStartDate { get; set; }

        public DateTime JobEndDate { get; set; }

        public decimal PricePerHour { get; set; }

        public decimal TotalPricePerDay { get; set; }

        public int WorkHours { get; set; }

        public Location JobLocation { get; set; }


        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<ContractsCustomerSupplier, ConfrimContractVm>()
                .ForMember(bm => bm.ContracId, cfg => cfg.MapFrom(ccs => ccs.ContractId));

            // supplier mappings 

            mapper.CreateMap<ContractsCustomerSupplier, ConfrimContractVm>()
                .ForMember(bm => bm.SupplierFirstName, cfg => cfg.MapFrom(ccs => ccs.Supplier.FirstName))
                .ForMember(bm => bm.SupplierLastName, cfg => cfg.MapFrom(ccs => ccs.Supplier.LastName))
                .ForMember(bm => bm.SupplierPhoneNumber, cfg => cfg.MapFrom(ccs => ccs.Supplier.PhoneNumber))
                .ForMember(bm => bm.PricePerHour, cfg => cfg.MapFrom(ccs => ccs.Supplier.PricePerHour));

            // customer mappings 

            mapper.CreateMap<ContractsCustomerSupplier, ConfrimContractVm>()
                .ForMember(bm => bm.CustomerLastName, cfg => cfg.MapFrom(ccs => ccs.Customer.FirstName))
                .ForMember(bm => bm.CustomerLastName, cfg => cfg.MapFrom(ccs => ccs.Customer.LastName))
                .ForMember(bm => bm.CustomerPhoneNumber, cfg => cfg.MapFrom(ccs => ccs.Customer.PhoneNumber));


            // job mappings 

            mapper.CreateMap<ContractsCustomerSupplier, ConfrimContractVm>()
                .ForMember(bm => bm.JobCategory, cfg => cfg.MapFrom(ccs => ccs.Contract.Job.FieldOfWork))
                .ForMember(bm => bm.JobTitle, cfg => cfg.MapFrom(ccs => ccs.Contract.Job.Title))
                .ForMember(bm => bm.JobStartDate, cfg => cfg.MapFrom(ccs => ccs.Contract.Job.JobStartDate))
                .ForMember(bm => bm.JobEndDate, cfg => cfg.MapFrom(ccs => ccs.Contract.Job.JobEndDate))
                .ForMember(bm => bm.PricePerHour, cfg => cfg.MapFrom(ccs => ccs.Contract.PricePerHour))
                .ForMember(bm => bm.TotalPricePerDay, cfg => cfg.MapFrom(ccs => ccs.Contract.TotalPricePerDay))
                .ForMember(bm => bm.WorkHours, cfg => cfg.MapFrom(ccs => ccs.Contract.TotalHours))
                .ForMember(bm => bm.JobLocation, cfg => cfg.MapFrom(ccs => ccs.Contract.Job.Location));
        }
    }
}
