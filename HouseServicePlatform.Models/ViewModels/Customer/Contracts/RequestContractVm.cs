﻿namespace HouseServicePlatform.Models.ViewModels.Customer.Contracts
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;

    public class RequestContractVm : IHaveCustomMapping
    {

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, RequestContractVm>()

                .ForMember(vm => vm.SupplierFirstName, cfg => cfg.MapFrom(ocs => ocs.Supplier.FirstName))
                .ForMember(vm => vm.SupplierLastName, cfg => cfg.MapFrom(ocs => ocs.Supplier.LastName));

        }
    }
}
