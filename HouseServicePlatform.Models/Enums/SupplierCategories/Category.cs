﻿namespace HouseServicePlatform.Models.Enums.SupplierCategories
{
    public enum Category
    {
        HouseKeeper = 0,
        Gardner = 1,
        Painting = 2,
        Plumbing = 3,
        Electrical = 4,
        HouseCleaning = 5,
        MovingHelp = 6,
        FurnitureAssembly = 7,

    }
}
