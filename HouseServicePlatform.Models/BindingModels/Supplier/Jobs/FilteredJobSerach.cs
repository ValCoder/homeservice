﻿namespace HouseServicePlatform.Models.BindingModels.Supplier.Jobs
{
    using System.ComponentModel.DataAnnotations;
    using Enums.SupplierCategories;
    public class FilteredJobSerach
    {

        [Display(Name = "Service Type")]
        public Category FieldOfWork { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

    }
}
