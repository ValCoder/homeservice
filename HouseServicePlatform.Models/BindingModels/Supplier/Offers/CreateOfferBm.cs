﻿namespace HouseServicePlatform.Models.BindingModels.Supplier.Offers
{
    using AutoMapper;
    using Common.Mapping;
    using DataModels;

    using Enums.SupplierCategories;

    using System.ComponentModel.DataAnnotations;

    public class CreateOfferBm : IHaveCustomMapping
    {
        public int jobId { get; set; }

        [Required]
        public string OfferTitle { get; set; }

        [Required]
        public string Content { get; set; }

        public Category JobCategory { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<JobService, CreateOfferBm>()
                .ForMember(vm => vm.CustomerFirstName, cfg => cfg.MapFrom(j => j.Customer.FirstName))
                .ForMember(vm => vm.CustomerLastName, cfg => cfg.MapFrom(j => j.Customer.LastName))
                .ForMember(vm => vm.jobId, cfg => cfg.MapFrom(j => j.Id))
                .ForMember(vm => vm.JobCategory, cfg => cfg.MapFrom(j => j.FieldOfWork));
        }
    }
}
