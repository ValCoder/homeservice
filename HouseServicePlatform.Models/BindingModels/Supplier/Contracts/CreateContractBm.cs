﻿namespace HouseServicePlatform.Models.BindingModels.Supplier.Contracts
{
    using System.ComponentModel.DataAnnotations;
    using DataModels;
    using System;
    using Enums.SupplierCategories;
    using AutoMapper;
    using Common.Mapping;

    public class CreateContractBm : IHaveCustomMapping
    {
        public int OfferId { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string SupplierFirstName { get; set; }

        public string SupplierLastName { get; set; }

        public string SupplierEmail { get; set; }

        public string SupplierPhoneNumber { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string CustomerEmail { get; set; }

        public Category JobCategory { get; set; }

        public string JobTitle { get; set; }

        [Required]
        [Display(Name = "Job Start Date")]
        public DateTime JobStartDate { get; set; }

        [Required]
        [Display(Name = "Job End Date")]
        public DateTime JobEndDate { get; set; }

        [Required]
        [Display(Name = "Price Per Hour")]
        public decimal PricePerHour { get; set; }

        [Required]
        [Display(Name = "Total Price Per Day")]
        public decimal TotalPricePerDay { get; set; }

        [Required]
        [Display(Name = "Daily Work Hours")]
        [Range(1,10)]
        public int WorkHours { get; set; }

        public Location JobLocation { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<OffersCustomerSupplier, CreateContractBm>()
                .ForMember(bm => bm.OfferId, cfg => cfg.MapFrom(ocs => ocs.OfferId));
            // supplier mappings 

            mapper.CreateMap<OffersCustomerSupplier, CreateContractBm>()
                .ForMember(bm => bm.SupplierFirstName, cfg => cfg.MapFrom(ocs => ocs.Supplier.FirstName))
                .ForMember(bm => bm.SupplierLastName, cfg => cfg.MapFrom(ocs => ocs.Supplier.LastName))
                .ForMember(bm => bm.SupplierPhoneNumber, cfg => cfg.MapFrom(ocs => ocs.Supplier.PhoneNumber))
                .ForMember(bm => bm.PricePerHour, cfg => cfg.MapFrom(ocs => ocs.Supplier.PricePerHour));

            // customer mappings 

            mapper.CreateMap<OffersCustomerSupplier, CreateContractBm>()
                .ForMember(bm => bm.CustomerLastName, cfg => cfg.MapFrom(ocs => ocs.Customer.FirstName))
                .ForMember(bm => bm.CustomerLastName, cfg => cfg.MapFrom(ocs => ocs.Customer.LastName))
                .ForMember(bm => bm.CustomerPhoneNumber, cfg => cfg.MapFrom(ocs => ocs.Customer.PhoneNumber));


            // job mappings 

            mapper.CreateMap<OffersCustomerSupplier, CreateContractBm>()
                .ForMember(bm => bm.JobCategory, cfg => cfg.MapFrom(ocs => ocs.Job.FieldOfWork))
                .ForMember(bm => bm.JobTitle, cfg => cfg.MapFrom(ocs => ocs.Job.Title))
                .ForMember(bm => bm.JobLocation, cfg => cfg.MapFrom(ocs => ocs.Job.Location));

        }
    }
}
