﻿namespace HouseServicePlatform.Models.BindingModels.Customers.Jobs
{

    using AutoMapper;
    using Common.Mapping;
    using DataModels;
    using Enums.SupplierCategories;
    using System.ComponentModel.DataAnnotations;

    public class CreateJobBm : IMapFrom<JobService>, IHaveCustomMapping
    {
        [Required]
        [MaxLength(50)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [MaxLength(4000)]
        [Display(Name = "Describe your needs")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Type of the job")]
        public Category FieldOfWork { get; set; }

        [Required]
        [MaxLength(20)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required]
        [MaxLength(20)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [MaxLength(200)]
        [Display(Name = "Neighborhood Name")]
        public string Neighborhood { get; set; }

        [Required]
        [MaxLength(200)]
        [Display(Name = "Street Name")]
        public string StreetName { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        [Display(Name = "Street Number")]
        public int StreetNumber { get; set; }

        [Required]
        [MaxLength(20)]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        public void ConfigureMapping(Profile mapper)
        {
            mapper.CreateMap<JobService, CreateJobBm>()
                .ForMember(c => c.City, cfg => cfg.MapFrom(j => j.Location.City))
                .ForMember(c => c.Country, cfg => cfg.MapFrom(j => j.Location.Country))
                .ForMember(c => c.Neighborhood, cfg => cfg.MapFrom(j => j.Location.NeighborhoodName))
                .ForMember(c => c.StreetName, cfg => cfg.MapFrom(j => j.Location.StreetName))
                .ForMember(c => c.StreetNumber, cfg => cfg.MapFrom(j => j.Location.StreetNumber))
                .ForMember(c => c.PostalCode, cfg => cfg.MapFrom(j => j.Location.PostalCode));

            mapper.CreateMap<JobService, EditJobBm>()
                .ForMember(c => c.City, cfg => cfg.MapFrom(j => j.Location.City))
                .ForMember(c => c.Country, cfg => cfg.MapFrom(j => j.Location.Country))
                .ForMember(c => c.Neighborhood, cfg => cfg.MapFrom(j => j.Location.NeighborhoodName))
                .ForMember(c => c.StreetName, cfg => cfg.MapFrom(j => j.Location.StreetName))
                .ForMember(c => c.StreetNumber, cfg => cfg.MapFrom(j => j.Location.StreetNumber))
                .ForMember(c => c.PostalCode, cfg => cfg.MapFrom(j => j.Location.PostalCode));

        }
    }
}
