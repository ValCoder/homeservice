﻿namespace HouseServicePlatform.Web.Areas.Supplier.Controllers
{
    using Common.Constants;
    using HouseServicePlatform.Services.Shared.OffersController.Interfaces;
    using HouseServicePlatform.Services.Supplier.HomeController.Interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.BindingModels.Supplier.Offers;
    using Models.DataModels;
    using Models.ViewModels.Supplier.Offers;
    using NToastNotify;
    using System.Collections.Generic;
    using System.Threading.Tasks;


    [Route("offers")]
    public class OffersSupplierController : SupplierBaseController
    {
        private readonly IOffersControllerService service;
        private readonly IHomeControllerService homeJobsService;


        public OffersSupplierController(
            IOffersControllerService service,
            IToastNotification toastNotification,
            UserManager<User> userManager,
            IHomeControllerService homeJobsService) : base(userManager, toastNotification)
        {
            this.service = service;
            this.homeJobsService = homeJobsService;
        }

        [HttpGet]
        [Route("myoffers")]
        public async Task<IActionResult> AllOffers()
        {
            string supplierId = base.GetUserId();

            ICollection<AllOffersSupplierVm> offers = await this.service.GetAllOffersForSupplierAsync(supplierId);

            return View(offers);
        }

        [HttpGet]
        [Route("read/{id?}")]
        public async Task<IActionResult> ReadOffer(int id)
        {
            string supplierId = base.GetUserId();

            bool supplierCanReadOffer = await this.service.SupplierCanReadOfferAsync(id, supplierId);

            if (!supplierCanReadOffer)
            {
                base.AddErrorToastMessage("You cannot read this offer.");
                return RedirectToAction(nameof(AllOffers));
            }

            ReadOffer readOfferModel = await this.service.GetOfferToReadAsync(id, supplierId);

            return View(readOfferModel);

        }

        [HttpGet]
        [Route("create/{id?}")]
        public async Task<IActionResult> CreateOffer(int id)
        {
            bool jobExists = await this.homeJobsService.JobExists(id);

            if (!jobExists)
            {
                base.AddErrorToastMessage("Job not found.");
                return RedirectToAction(nameof(HomeController.AllJobs), "Home", new { area = WebConstants.SupplierArea });
            }


            bool offerIsAlreadySended = await this.service.IsOfferAlreadySendedAsync(id);

            if (offerIsAlreadySended)
            {
                base.AddErrorToastMessage("You have already submitted an offer to this client.");

                return RedirectToAction(nameof(HomeController.AllJobs), "Home", new { area = WebConstants.SupplierArea });
            }

            CreateOfferBm model = await this.service.GetCreateOfferVmAsync(id);

            return View(model);
        }

        [HttpPost]
        [Route("create/{id?}")]
        public async Task<IActionResult> CreateOffer(CreateOfferBm offerModel)
        {
            bool jobsExists = await this.homeJobsService.JobExists(offerModel.jobId);

            if (!jobsExists)
            {
                base.AddErrorToastMessage("Job not found.");

                return RedirectToAction(nameof(HomeController.AllJobs), "Home", new { area = WebConstants.SupplierArea });
            }

            string supplierId = base.GetUserId();

            User supplier = await base.GetUserAsync(supplierId);

            if (!ModelState.IsValid)
            {
                return View(offerModel);
            }

            await this.service.CreateOfferAsync(supplierId, supplier, offerModel);

            base.AddSuccessToastMessage("Offer successfully sent.");

            return RedirectToAction(nameof(HomeController.AllJobs), "Home", new { area = WebConstants.SupplierArea });

        }
    }
}