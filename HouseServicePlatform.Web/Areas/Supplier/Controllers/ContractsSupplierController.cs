﻿namespace HouseServicePlatform.Web.Areas.Supplier.Controllers
{
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.BindingModels.Supplier.Contracts;
    using Models.DataModels;
    using NToastNotify;
    using System;
    using System.Threading.Tasks;
    using HouseServicePlatform.Services.Shared.ContractsController.Interfaces;

    [Route("contracts")]
    public class ContractsSupplierController : SupplierBaseController
    {
        private readonly IContractsControllerService service;

        public ContractsSupplierController(UserManager<User> userManager, IToastNotification toastNotification, IContractsControllerService service) : base(userManager, toastNotification)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> AllContractRequests()
        {
            string supplierId = base.GetUserId();

            var allContractRequsts = await this.service.GetAllContractRequestsForSupplierAsync(supplierId);


            return View(allContractRequsts);
        }

        [HttpGet]
        [Route("create/{id?}")]
        public async Task<IActionResult> CreateContract(int id)
        {
            string supplierId = base.GetUserId();

            bool contractIsForThisSupplier = await this.service.ContractIsForThisUserAsync(supplierId, id);

            if (!contractIsForThisSupplier)
            {
                base.AddErrorToastMessage("You cannot confirm this request");

                return RedirectToAction(nameof(Web.Controllers.HomeController.Index), "Home", new { area = string.Empty });

            }

            var contractBindModel = await this.service.GetCreateContractBindModelAsync(supplierId);
            contractBindModel.JobStartDate = DateTime.UtcNow.AddDays(1);
            contractBindModel.JobEndDate = DateTime.UtcNow.AddDays(2);

            return View(contractBindModel);
        }

        [HttpPost]
        [Route("create/{id?}")]
        public async Task<IActionResult> CreateContract(CreateContractBm contractBm)
        {
            string supplierId = base.GetUserId();

            bool contractIsForThisSupplier = await this.service.ContractIsForThisUserAsync(supplierId, contractBm.OfferId);

            bool supplierHasCreatedContract = await this.service.SupplierHasCreatedContract(supplierId, contractBm.OfferId);

            if (!contractIsForThisSupplier)
            {
                base.AddErrorToastMessage("You cannot confirm this request");

                return RedirectToAction(nameof(Web.Controllers.HomeController.Index), "Home", new { area = string.Empty });

            }

            if (supplierHasCreatedContract)
            {
                AddErrorToastMessage("You have allready sent a contract to this client.");
                return RedirectToAction(nameof(AllContractRequests));
            }

            if (!ModelState.IsValid)
            {
                return View(contractBm);
            }

            await this.service.CreateContractAsync(supplierId, contractBm);

            base.AddSuccessToastMessage("You have successfully sent this contract.");

            return RedirectToAction(nameof(AllContractRequests));
        }
    }
}