﻿namespace HouseServicePlatform.Web.Areas.Supplier.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.DataModels;
    using NToastNotify;
    using System.Threading.Tasks;
    using static Common.Constants.WebConstants;


    [Area(SupplierArea)]
    [Authorize(Roles = SupplierRole)]
    public class SupplierBaseController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public SupplierBaseController(UserManager<User> userManager, IToastNotification toastNotification)
        {
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }

        protected void AddErrorToastMessage(string message)
        {
            this.toastNotification.AddErrorToastMessage(message);
        }

        protected void AddSuccessToastMessage(string message)
        {
            this.toastNotification.AddSuccessToastMessage(message);
        }

        protected string GetUserId()
        {
            return this.userManager.GetUserId(this.User);
        }

        protected async Task<User> GetUserAsync(string userId)
        {

            return await this.userManager.FindByIdAsync(userId);
        }
    }
}