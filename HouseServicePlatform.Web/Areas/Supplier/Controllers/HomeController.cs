﻿namespace HouseServicePlatform.Web.Areas.Supplier.Controllers
{
    using HouseServicePlatform.Services.Supplier.HomeController.Interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.BindingModels.Supplier.Jobs;
    using Models.DataModels;
    using Models.ViewModels.Supplier.Jobs;
    using NToastNotify;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [Route("home")]
    public class HomeController : SupplierBaseController
    {
        private readonly IHomeControllerService service;

        public HomeController(
            IHomeControllerService service,
            IToastNotification toastNotification,
            UserManager<User> userManager) : base(userManager, toastNotification)
        {
            this.service = service;

        }

        [HttpGet]
        [Route("jobs")]
        public async Task<IActionResult> AllJobs(FilteredJobSerach filteredSearch, bool filterSearch)
        {
            if (filterSearch)
            {
                ICollection<AllJobsVmSupplier> filteredJobs = await this.service.FilterFobsAsync(filteredSearch);

                if (!filteredJobs.Any())
                {
                    base.AddErrorToastMessage("No Jobs Found.");
                }

                else
                {
                    int jobsCount = filteredJobs.Count;

                    base.AddSuccessToastMessage($"Jobs found {jobsCount}");
                }

                return View(filteredJobs);
            }

            ICollection<AllJobsVmSupplier> jobs = await this.service.GetAllJobsAsync();

            return View(jobs);
        }

        [HttpGet]
        [Route("details/{id?}")]
        public async Task<IActionResult> JobDetails(int id)
        {
            bool jobExists = await this.service.JobExists(id);

            if (!jobExists)
            {
                base.AddErrorToastMessage("Job not found.");
                return RedirectToAction(nameof(AllJobs));
            }

            JobDetailsVm job = await this.service.GetJobDetailsAsync(id);

            return View(job);
        }
    }
}