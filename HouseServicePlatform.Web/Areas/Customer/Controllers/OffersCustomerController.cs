﻿namespace HouseServicePlatform.Web.Areas.Customer.Controllers
{
    using HouseServicePlatform.Services.Shared.OffersController.Interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.DataModels;
    using Models.ViewModels.Customer.Offers;
    using NToastNotify;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.ViewModels.Customer.Contracts;

    [Route("offers")]
    public class OffersCustomerController : CustomerBaseController
    {
        private readonly IOffersControllerService service;


        public OffersCustomerController(
            IOffersControllerService service,
            UserManager<User> userManager,
            IToastNotification toastNotification) : base(userManager, toastNotification)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> AllOffers()
        {
            string customerId = base.GetUserId();

            User user = await base.GetUserAsync(customerId);

            ICollection<AllOffersCustomerVm> allOfers = await this.service.GetAllOffersForCustomerAsync(user.Id);

            return View(allOfers);

        }

        [HttpGet]
        [Route("details/{id?}")]
        public async Task<IActionResult> DetailsOffer(int id)
        {
            string customerId = base.GetUserId();

            bool customerCanReadOffer = await this.service.CustomerCanReadOfferAsync(id, customerId);

            if (!customerCanReadOffer)
            {
                base.AddErrorToastMessage("You cannot read this offer.");
                return RedirectToAction(nameof(AllOffers));
            }

            OfferCustomerDetails detailOffer = await this.service.GetCustomerOfferDetails(id, customerId);

            return View(detailOffer);
        }

        [HttpGet]
        [Route("requestcontract/{id?}")]
        public async Task<IActionResult> RequestContract(int id)
        {
            string customerId = base.GetUserId();

            bool offerIsForCustomer = await this.service.CustomerCanReadOfferAsync(id, customerId);

            if (!offerIsForCustomer)
            {
                base.AddErrorToastMessage("You cannot send contract request for this offer.");
                return RedirectToAction(nameof(AllOffers));
            }

            RequestContractVm model = await this
                 .service.GetRequestContractVm(id, customerId);

            if (model == null)
            {
                base.AddErrorToastMessage("You have already submited a request.");
                return RedirectToAction(nameof(AllOffers));
            }

            return View(model);

        }

        [HttpPost]
        [Route("requestcontract/{id?}")]
        public async Task<IActionResult> ConfirmRequestContract(int id)
        {
            string customerId = base.GetUserId();

            bool customerCanConfirmContract = await this.service.CustomerCanReadOfferAsync(id, customerId);

            if (!customerCanConfirmContract)
            {
                base.AddErrorToastMessage("You cannot send contract request for this offer.");

                return RedirectToAction(nameof(AllOffers));
            }

            await this.service.ConfirmRequestToSupplier(id, customerId);

            base.AddSuccessToastMessage("Request subbmited successfully.");

            return RedirectToAction(nameof(AllOffers));

        }
    }
}