﻿namespace HouseServicePlatform.Web.Areas.Customer.Controllers
{
    using Models.DataModels;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using NToastNotify;
    using System.Threading.Tasks;
    using HouseServicePlatform.Services.Shared.ContractsController.Interfaces;
    using System.Collections.Generic;
    using Models.ViewModels.Customer.Contracts;


    [Route("contracts")]
    public class ContractsCustomerController : CustomerBaseController
    {
        private readonly IContractsControllerService service;

        public ContractsCustomerController(UserManager<User> userManager, IToastNotification toastNotification, IContractsControllerService service) : base(userManager, toastNotification)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("mycontracts")]
        public async Task<IActionResult> All()
        {
            string customerId = base.GetUserId();

            ICollection<AllContracts> contracts = await this.service.GetAllContractsForCustomerAsync(customerId);

            return View(contracts);
        }

        [HttpGet]
        [Route("confirm/{id?}")]
        public async Task<IActionResult> GetContractToConfirm(int id)
        {
            string customerId = base.GetUserId();

            bool customerIsAthorizedForContract = await this.service.CustomerIsAuthortizedForThisContract(customerId, id);

            if (!customerIsAthorizedForContract)
            {
                base.AddErrorToastMessage("You cannot confirm this contract.");

                return RedirectToAction(nameof(All));
            }

            var model = await this.service.GetContractVmForCustomer(customerId, id);

            if (model != null)
            {
                return View(model);

            }

            base.AddErrorToastMessage("You have already confirmed this contract.");

            return RedirectToAction(nameof(All));
        }

        [HttpPost]
        [Route("confrim/{id?}")]
        public async Task<IActionResult> ConfirmContract(int id)
        {
            string customerId = base.GetUserId();

            bool customerIsAthorizedForContract = await this.service.CustomerIsAuthortizedForThisContract(customerId, id);

            if (!customerIsAthorizedForContract)
            {
                base.AddErrorToastMessage("You cannot confirm this contract.");

                return RedirectToAction(nameof(All));
            }

            await this.service.ConfirmContract(customerId, id);

            base.AddSuccessToastMessage("You have successfully confirmed this contract.");

            return RedirectToAction(nameof(All));
        }

        [HttpGet]
        [Route("details/{id?}")]
        public async Task<IActionResult> DetailsContract(int id)
        {
            string customerId = base.GetUserId();

            bool customerIsAthorizedForContract = await this.service.CustomerIsAuthortizedForThisContract(customerId, id);

            if (!customerIsAthorizedForContract)
            {
                base.AddErrorToastMessage("You cannot see this contract.");

                return RedirectToAction(nameof(All));
            }

            var model = await this.service.ContractDetails(customerId, id);

            return PartialView("_ContractDetails", model);
        }
    }
}