﻿namespace HouseServicePlatform.Web.Areas.Customer.Controllers
{
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.BindingModels.Customers.Jobs;
    using Models.DataModels;
    using Models.ViewModels.Customer.Jobs;
    using NToastNotify;
    using Services.Customer.JobServiceController.Interfaces;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Route("jobs")]
    public class JobsController : CustomerBaseController
    {
        private readonly IJobsServiceController service;

        public JobsController(
            IJobsServiceController service,
            UserManager<User> userManager,
            IToastNotification toastNotification) : base(userManager, toastNotification)
        {
            this.service = service;

        }


        [HttpGet]
        [Route("myjobs")]
        public async Task<IActionResult> All()
        {
            string userId = base.GetUserId();

            ICollection<AllJobsVmCustomer> viewModel = await this.service.GetAllJobsAsync(userId);

            return View(viewModel);

        }

        [HttpGet]
        [Route("create")]
        public async Task<IActionResult> CreateJob()
        {
            string userId = base.GetUserId();

            CreateJobBm vieModel = await this.service.GetCreateJoViewModelAsync(userId);

            return View(vieModel);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateJob(CreateJobBm bindModel)
        {
            if (!ModelState.IsValid)
            {
                return View(bindModel);
            }

            string userId = base.GetUserId();

            await this.service.CreateJobServiceAsync(bindModel, userId);

            base.AddSuccessToastMessage("You have successfully posted a job.");

            return RedirectToAction(nameof(All));
        }

        [HttpGet]
        [Route("edit/{id?}")]
        public async Task<IActionResult> Edit(int id)
        {
            string customerId = base.GetUserId();

            bool customerIsAuthorsOfJob = await this.service.CustomerIsAuthorOfJobAsync(customerId, id);

            if (!customerIsAuthorsOfJob)
            {
                base.AddErrorToastMessage("You cannot edit this job.");
                return RedirectToAction(nameof(All));
            }

            var model = await this.service.EditAsync(id, customerId);

            if (model == null)
            {
                base.AddErrorToastMessage("Jobs taken by supplier cannot be edited.");
                return RedirectToAction(nameof(All));
            }

            return View(model);

        }

        [HttpPost]
        [Route("edit/{id?}")]
        public async Task<IActionResult> Edit(EditJobBm editModel)
        {
            string customerId = base.GetUserId();

            var customerIsAuthorOfJob = await this.service.CustomerIsAuthorOfJobAsync(customerId, editModel.Id);

            if (!customerIsAuthorOfJob)
            {
                base.AddErrorToastMessage("You cannot edit this job.");
                return RedirectToAction(nameof(All));

            }

            if (!ModelState.IsValid)
            {
                return View(editModel);
            }

            await this.service.EditJobAsync(editModel, customerId);

            base.AddSuccessToastMessage($"Sucessfully updated your job {editModel.Title}.");

            return RedirectToAction(nameof(All));

        }

        [HttpGet]
        [Route("delete/{id?}")]
        public async Task<IActionResult> Delete(int id)
        {
            string customerId = base.GetUserId();

            bool customerIsAuthorsOfJob = await this.service.CustomerIsAuthorOfJobAsync(customerId, id);

            if (!customerIsAuthorsOfJob)
            {
                base.AddErrorToastMessage("You cannot delete this job.");
                return RedirectToAction(nameof(All));
            }

            var model = await this.service.GetDeleteJobVmAsync(id, customerId);

            if (model == null)
            {
                base.AddErrorToastMessage("Jobs taken by supplier cannot be deleted.");
                return RedirectToAction(nameof(All));
            }

            return View(model);
        }

        [HttpPost]
        [Route("destory/{id?}")]
        public async Task<IActionResult> Destroy(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            string customerId = base.GetUserId();

            var customerIsAuthorOfJob = await this.service.CustomerIsAuthorOfJobAsync(customerId, id);

            if (!customerIsAuthorOfJob)
            {
                base.AddErrorToastMessage("You cannot delete this job.");
                return RedirectToAction(nameof(All));
            }

            await this.service.DestroyJobAsync(id, customerId);

            base.AddSuccessToastMessage("Successfully deleted.");

            return RedirectToAction(nameof(All));
        }

    }
}