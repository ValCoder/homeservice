﻿namespace HouseServicePlatform.Web.Areas.Customer.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Models.DataModels;
    using NToastNotify;
    using System.Threading.Tasks;
    using static Common.Constants.WebConstants;

    [Authorize(Roles = CustomerRole)]
    [Area(CustomerArea)]
    public class CustomerBaseController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public CustomerBaseController(UserManager<User> userManager, IToastNotification toastNotification)
        {
            this.userManager = userManager;
            this.toastNotification = toastNotification;

        }

        protected void AddErrorToastMessage(string message)
        {
            this.toastNotification.AddErrorToastMessage(message);
        }

        protected void AddSuccessToastMessage(string message)
        {
            this.toastNotification.AddSuccessToastMessage(message);
        }

        protected string GetUserId()
        {
            return this.userManager.GetUserId(this.User);
        }

        protected async Task<User> GetUserAsync(string userId)
        {

            return await this.userManager.FindByIdAsync(userId);
        }
    }
}