﻿function getLocation(event) {
    event.preventDefault();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function getPosition(position) {

    //x.innerHTML = "Latitude: " + position.coords.latitude +
    //    "<br>Longitude: " + position.coords.longitude;
   

    $.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyBjnBRKg3olX32PMfVds-UysN4_G_sFZec`)
        .then(res => getLocationData(res));

    $('#latitude').val(position.coords.latitude);
    $('#longitude').val(position.coords.longitude);
}

function getLocationData(res) {

    let results = res.results;

    if (results) {

        const locationData = [];

        const streetnumnber = '';
        const streetName = '';
        const neighborhoodName = '';
        const city = '';
        const district = '';
        const state = '';
        const country = '';
        const postalCode = '';

        locationData.push(streetnumnber, streetName, neighborhoodName, city, district, state, country, postalCode);

        if (results[0]) {
            for (let i in results[0].address_components) {
                console.log(results[0].address_components[i].long_name);
                locationData[i] = results[0].address_components[i].long_name;
            }
            console.log(locationData);

            $('#country').val(locationData[6]);
            $('#state').val(locationData[5]);
            $('#district').val(locationData[4]);
            $('#city').val(locationData[3]);
            $('#neighborhoodName').val(locationData[2]);
            $('#streetName').val(locationData[1]);
            $('#streetNumber').val(locationData[0]);
            $('#postalCode').val(locationData[7]);


        }

    } else {
        console.log(`No data for location`);
    }
}