﻿function calculate() {
    $(document).ready(function () {
        //  let jobStartDate = $('#jobStartDate').val();
        // let jobEndDate = $('#jobEndDate').val();
        let pricePerHour = $('#pricePerHour').val();

        //   let hours = Math.abs(Date.parse(jobStartDate) - Date.parse(jobEndDate)) / 36e5;

        let workHoursADay = $("#workHours").val();

        let totalSum = parseFloat(pricePerHour) * parseFloat(workHoursADay);

        if (totalSum === NaN) {
            $('#totalPrice').val('Not a number');
        } else {
            $('#totalPrice').val(totalSum.toFixed(2));
        }

    });
}

