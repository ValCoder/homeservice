﻿namespace HouseServicePlatform.Web.Infrastructure.Extensions
{
    using Data;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Models.DataModels;
    using System.Threading.Tasks;
    using static Common.Constants.WebConstants;

    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseDatabaseMigration(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<HouseServiceDbContext>().Database.Migrate();

                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();


                Task.Run(async () =>
                {

                    var roles = new[]
                    {
                                    AdministratorRole,
                                    SupplierRole,
                                    CustomerRole

                    };

                    foreach (var role in roles)
                    {
                        var roleExists = await roleManager.RoleExistsAsync(role);

                        if (!roleExists)
                        {
                            await roleManager.CreateAsync(new IdentityRole
                            {
                                Name = role

                            });
                        }

                    }

                    var adminEmail = "admin@homeservices.com";

                    var adminUser = await userManager.FindByEmailAsync(adminEmail);

                    if (adminUser == null)
                    {
                        adminUser = new User()
                        {
                            Email = adminEmail,
                            UserName = "Admin",

                        };
                        await userManager.CreateAsync(adminUser, "parolataAdmin");

                        await userManager.AddToRoleAsync(adminUser, AdministratorRole);
                    }
                })
                .Wait();

            }

            return app;
        }
    }
}

