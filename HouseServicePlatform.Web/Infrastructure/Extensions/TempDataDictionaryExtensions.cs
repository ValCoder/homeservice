﻿namespace HouseServicePlatform.Web.Infrastructure.Extensions
{
    using static Common.Constants.WebConstants;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;

    public static class TempDataDictionaryExtensions
    {
        public static void AddSuccessMessage(this ITempDataDictionary tempData, string message)
        {
            tempData[TemDataSuccessMesageKey] = message;
        }

        public static void AddErrorMessage(this ITempDataDictionary tempData, string message)
        {
            tempData[TemDataErrorMesageKey] = message;
        }
    }
}
