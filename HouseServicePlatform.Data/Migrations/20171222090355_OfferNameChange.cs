﻿
namespace HouseServicePlatform.Data.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class OfferNameChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsSended",
                table: "Offers",
                newName: "IsSent");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsSent",
                table: "Offers",
                newName: "IsSended");
        }
    }
}
