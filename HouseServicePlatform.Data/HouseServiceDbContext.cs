﻿namespace HouseServicePlatform.Data
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Models.DataModels;


    public class HouseServiceDbContext : IdentityDbContext<User>
    {
        public HouseServiceDbContext(DbContextOptions<HouseServiceDbContext> options)
            : base(options)
        {
        }

        public DbSet<Contract> Contracts { get; set; }

        public DbSet<OffersCustomerSupplier> OffersCustomerSupplier { get; set; }

        public DbSet<Offer> Offers { get; set; }

        public DbSet<ContractsCustomerSupplier> ContractsCustomersSupplier { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<JobService> JobServices { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Contract Customer Supplier 
            builder.Entity<ContractsCustomerSupplier>()
                .HasKey(c => new { c.ContractId, c.CustomerId });

            builder.Entity<User>()
                .HasMany(c => c.ContractsCustomersSupplier)
                .WithOne(c => c.Customer)
                .HasForeignKey(c => c.CustomerId);

            builder.Entity<ContractsCustomerSupplier>()
                .HasOne(ccs => ccs.Contract)
                .WithOne(c => c.ContractsCustomerSupplier);

            builder.Entity<Contract>()
                .HasOne(c => c.ContractsCustomerSupplier)
                .WithOne(ccs => ccs.Contract);


            // Offers Customer Supplier 

            builder.Entity<OffersCustomerSupplier>()
                .HasKey(c => new { c.OfferId, c.CustomerId });

            builder.Entity<User>()
                .HasMany(o => o.OffersCustomersSupplier)
                .WithOne(u => u.Customer)
                .HasForeignKey(o => o.CustomerId);

            builder.Entity<OffersCustomerSupplier>()
                .HasOne(c => c.Offer)
                .WithOne(c => c.OfferCustomerSupplier);

            builder.Entity<Offer>()
                .HasOne(o => o.OfferCustomerSupplier)
                .WithOne(o => o.Offer);


            // Locations

            builder.Entity<User>()
                .HasMany(u => u.Locations)
                .WithOne(l => l.User)
                .HasForeignKey(l => l.UserId);

            builder.Entity<Location>()
                .HasOne(u => u.User);

            // Reviews 

            builder.Entity<Review>()
                .HasOne(c => c.Author)
                .WithMany(c => c.Reviews)
                .HasForeignKey(c => c.AuthordId);

            // Jobs

            builder.Entity<JobService>()
                .HasOne(c => c.Customer)
                .WithMany(c => c.JobServices)
                .HasForeignKey(c => c.CustomerId);


            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
