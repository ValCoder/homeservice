﻿namespace HouseServicePlatform.Services.Customer.JobServiceController.Implementations
{
    using AutoMapper.QueryableExtensions;
    using Data;
    using Interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Models.BindingModels.Customers.Jobs;
    using Models.DataModels;
    using Models.ViewModels.Customer.Jobs;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class JobsServiceController : IJobsServiceController
    {
        private readonly HouseServiceDbContext db;
        private readonly UserManager<User> userManager;


        public JobsServiceController(HouseServiceDbContext db, UserManager<User> userManager)
        {
            this.userManager = userManager;
            this.db = db;
        }


        public async Task<CreateJobBm> GetCreateJoViewModelAsync(string userId)
        {
            var userLocation = await this.db.Locations.FirstOrDefaultAsync(l => l.UserId == userId);
            CreateJobBm viewModel = new CreateJobBm();

            if (userLocation != null)
            {
                viewModel = new CreateJobBm()
                {
                    City = userLocation.City,
                    PostalCode = userLocation.PostalCode,
                    Country = userLocation.Country,
                    Neighborhood = userLocation.NeighborhoodName,
                    StreetName = userLocation.StreetName,
                    StreetNumber = userLocation.StreetNumber,

                };
            }

            return viewModel;
        }


        public async Task CreateJobServiceAsync(CreateJobBm bindModel, string customerId)
        {
            var customer = await this.GetUserByIdAsync(customerId);

            Location jobLocation = new Location()
            {
                City = bindModel.City,
                Country = bindModel.Country,
                NeighborhoodName = bindModel.Neighborhood,
                StreetName = bindModel.StreetName,
                StreetNumber = bindModel.StreetNumber,
                PostalCode = bindModel.PostalCode,
                User = customer,
                UserId = customerId,
            };

            await this.CreateJobAsync(bindModel, jobLocation, customer, customerId);

        }

        public async Task<ICollection<AllJobsVmCustomer>> GetAllJobsAsync(string customerId)
        {
            var viewModel = await this
                 .db.JobServices
                 .Where(j => j.CustomerId == customerId)
                 .ProjectTo<AllJobsVmCustomer>()
                 .ToListAsync();

            return viewModel;
        }

        public async Task<EditJobBm> EditAsync(int jobId, string customerId)
        {
            var model = await this
                .db.JobServices
                .Where(job => job.CustomerId == customerId && job.Id == jobId && !job.IsJobTaken)
                .ProjectTo<EditJobBm>()
                .FirstOrDefaultAsync();

            return model;
        }

        public async Task<bool> CustomerIsAuthorOfJobAsync(string userId, int jobId)
        {
            var result = await this
                .db.JobServices
                .AnyAsync(job => job.Id == jobId
                 && job.CustomerId == userId);

            return result;
        }

        public async Task EditJobAsync(EditJobBm model, string cutomerId)
        {
            var jobToEdit = await this.db.JobServices
                .Where(job => job.Id == model.Id && job.CustomerId == cutomerId)
                .Include(c => c.Location)
                .FirstOrDefaultAsync();

            if (jobToEdit != null)
            {
                jobToEdit.Description = model.Description;
                jobToEdit.Title = model.Title;
                jobToEdit.FieldOfWork = model.FieldOfWork;
                jobToEdit.Location.City = model.City;
                jobToEdit.Location.Country = model.Country;
                jobToEdit.Location.NeighborhoodName = model.Neighborhood;
                jobToEdit.Location.StreetNumber = model.StreetNumber;
                jobToEdit.Location.StreetName = model.StreetName;
                jobToEdit.Location.PostalCode = model.PostalCode;
                await this.db.SaveChangesAsync();
            }
        }

        public async Task<DeleteJobVm> GetDeleteJobVmAsync(int jobId, string customerId)
        {
            DeleteJobVm model = await this
                .db.JobServices
                .Where(j => j.Id == jobId && j.CustomerId == customerId && !j.IsJobTaken)
                .ProjectTo<DeleteJobVm>()
                .FirstOrDefaultAsync();

            return model;
        }

        public async Task DestroyJobAsync(int jobId, string customerId)
        {

            var jobToDelete = await this.GetJobByIdAndCustomerIdAsync(jobId, customerId);

            this.db.JobServices.Remove(jobToDelete);

            await this.db.SaveChangesAsync();
        }

        #region Helper Methods

        /// <summary>
        /// Takes Job from db that belongs to customer with given id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="jobId"></param>
        /// <returns>JobService</returns>
        private async Task<JobService> GetJobByIdAndCustomerIdAsync(int jobId, string customerId)
        {
            return await this.
                db
                .JobServices
                .Where(j => j.Id == jobId && j.CustomerId == customerId).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>User</returns>
        private async Task<User> GetUserByIdAsync(string customerId)
        {
            return await this.userManager.FindByIdAsync(customerId);
        }

        /// <summary>
        /// Creates and saves data entity JobSerive to db.
        /// </summary>
        /// <param name="bindModel"></param>
        /// <param name="jobLocation"></param>
        /// <param name="customer"></param>
        /// <param name="customerid"></param>
        /// <returns></returns>
        private async Task CreateJobAsync(CreateJobBm bindModel, Location jobLocation, User customer, string customerid)
        {
            var job = new JobService()
            {
                Location = jobLocation,
                Customer = customer,
                CustomerId = customerid,
                Description = bindModel.Description,
                FieldOfWork = bindModel.FieldOfWork,
                Title = bindModel.Title,
                IsJobTaken = false,

            };

            await this.db.JobServices.AddAsync(job);
            await this.db.SaveChangesAsync();
        }

        #endregion

    }
}
