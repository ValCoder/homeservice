﻿namespace HouseServicePlatform.Services.Customer.JobServiceController.Interfaces
{
    using Models.BindingModels.Customers.Jobs;
    using Models.ViewModels.Customer.Jobs;
    using System.Collections.Generic;
    using System.Threading.Tasks;


    public interface IJobsServiceController
    {
        Task<CreateJobBm> GetCreateJoViewModelAsync(string customerId);

        Task CreateJobServiceAsync(CreateJobBm bindModel, string customerId);

        Task<ICollection<AllJobsVmCustomer>> GetAllJobsAsync(string customerId);

        Task<EditJobBm> EditAsync(int jobId, string customerId);

        Task<bool> CustomerIsAuthorOfJobAsync(string userId, int jobId);

        Task EditJobAsync(EditJobBm model, string cutomerId);

        Task<DeleteJobVm> GetDeleteJobVmAsync(int jobId, string customerId);

        Task DestroyJobAsync(int jobId, string customerId);


    }
}
