﻿namespace HouseServicePlatform.Services.Shared.ContractsController.Interfaces
{
    using Models.BindingModels.Supplier.Contracts;
    using Models.ViewModels.Supplier.Contracts;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models.ViewModels.Customer.Contracts;


    public interface IContractsControllerService
    {

        Task<ICollection<AllContractRequests>> GetAllContractRequestsForSupplierAsync(string supplierId);

        Task<ICollection<AllContracts>> GetAllContractsForCustomerAsync(string customerId);

        Task<bool> ContractIsForThisUserAsync(string supplierId, int offerId);

        Task<CreateContractBm> GetCreateContractBindModelAsync(string supplierId);

        Task<ConfrimContractVm> GetContractVmForCustomer(string customerId, int contractId);

        Task CreateContractAsync(string supplierId, CreateContractBm createContractBm);

        Task<bool> SupplierHasCreatedContract(string supplierId, int offerId);

        Task<bool> CustomerIsAuthortizedForThisContract(string cusutomerId, int contractId);

        Task ConfirmContract(string customerId, int contractId);

        Task<ConfrimContractVm> ContractDetails(string userId, int id);

    }
}
