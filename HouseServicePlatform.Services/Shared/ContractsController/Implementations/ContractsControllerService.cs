﻿namespace HouseServicePlatform.Services.Shared.ContractsController.Implementations
{
    using AutoMapper.QueryableExtensions;
    using Data;
    using Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Models.BindingModels.Supplier.Contracts;
    using Models.DataModels;
    using Models.ViewModels.Supplier.Contracts;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Models.ViewModels.Customer.Contracts;

    public class ContractsControllerService : IContractsControllerService
    {
        private readonly HouseServiceDbContext db;

        public ContractsControllerService(HouseServiceDbContext db)
        {
            this.db = db;
        }


        /// <summary>
        /// This method get all contracts of a supplier by given id
        /// and return collection of view models.
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns>Collection of AllContractRequests view models</returns>
        public async Task<ICollection<AllContractRequests>> GetAllContractRequestsForSupplierAsync(string supplierId)
        {
            ICollection<AllContractRequests> allContractsRequests = await this
                .db
                .OffersCustomerSupplier
                .Where(ocs => ocs.SupplierId == supplierId
                              && ocs.Offer.CustomerHasRequestedContract)
                         .OrderByDescending(ocs => ocs.OfferId)
                .ProjectTo<AllContractRequests>()
                .ToListAsync();

            return allContractsRequests;
        }

        public async Task<ICollection<AllContracts>> GetAllContractsForCustomerAsync(string customerId)
        {
            ICollection<AllContracts> allContratcs =
                await this.db
                    .ContractsCustomersSupplier
                    .Where(ccs => ccs.CustomerId == customerId)
                    .Select(ccs => new AllContracts()
                    {
                        JobTitle = ccs.Contract.Job.Title,
                        JobCategory = ccs.Contract.Job.FieldOfWork,
                        SupplierFirstName = ccs.Supplier.FirstName,
                        SupplierLastName = ccs.Supplier.LastName,
                        ContractId = ccs.ContractId,
                        IsSignedByCustomer = ccs.Contract.IsSignedByCustomer
                    })
                    .ToListAsync();

            return allContratcs;
        }

        /// <summary>
        /// This method check if supplier by id can contribute in contract
        /// check the db table OffersCustomerSupplier 
        /// if contains  supplier id, offer id and jobIsTaken to be false.
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="offerId"></param>
        /// <returns>bool</returns>
        public async Task<bool> ContractIsForThisUserAsync(string supplierId, int offerId)
        {
            bool isAuthorized = await this
                .db
                .OffersCustomerSupplier
                .AnyAsync(ocs => ocs.SupplierId == supplierId
                                 && ocs.Offer.CustomerHasRequestedContract
                                 && ocs.OfferId == offerId);

            return isAuthorized;
        }

        /// <summary>
        /// This method returns a bind model of type CreateContractBm
        /// wich is filled automatically with automapper
        /// Db returns single result from table OffersCustomerSupplier
        /// if conditions are matching
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns>CreateContractBm</returns>
        public async Task<CreateContractBm> GetCreateContractBindModelAsync(string supplierId)
        {
            if (supplierId == null)
            {
                return null;
            }

            return await this
                .db
                .OffersCustomerSupplier
                .Where(ocs => ocs.SupplierId == supplierId
                            && ocs.Offer.CustomerHasRequestedContract)
                .ProjectTo<CreateContractBm>()
                .FirstOrDefaultAsync();
        }

        public async Task<ConfrimContractVm> GetContractVmForCustomer(string customerId, int contractId)
        {
            return await this.db
                .ContractsCustomersSupplier
                .Where(ccs => ccs.CustomerId == customerId
                && ccs.ContractId == contractId
                && !ccs.Contract.IsSignedByCustomer)
                .ProjectTo<ConfrimContractVm>()
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// This method creates two entities to the db
        /// 1: Creates Contract entity and adds it to the Contracts table in db
        /// 2: Creates ContractsCustomerSupplier entity and adds the first entity to it, then saves it to the db
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="createContractBm"></param>
        /// <returns></returns>
        public async Task CreateContractAsync(string supplierId, CreateContractBm createContractBm)
        {
            // using one query 

            var ofcQueryByOfferIdSupplierId =
                this.db
                .OffersCustomerSupplier
                .Where(ocs => ocs.Offer.Id == createContractBm.OfferId
                && ocs.SupplierId == supplierId)
                .AsQueryable();

            // take the offer entity from db
            var offer = await ofcQueryByOfferIdSupplierId.Select(ocs => ocs.Offer).FirstOrDefaultAsync();

            // set to true so the supplier can create only one contract with this offerId 
            offer.SupplierHasSentContract = true;

            var job = await ofcQueryByOfferIdSupplierId
                .Select(ocs => ocs.Job).FirstOrDefaultAsync();

            job.JobEndDate = createContractBm.JobEndDate;
            job.JobStartDate = createContractBm.JobStartDate;

            Contract contract = new Contract()
            {
                Job = job,
                JobId = job.Id,
                IsSignedByCustomer = false,
                PricePerHour = createContractBm.PricePerHour,
                TotalPricePerDay = createContractBm.TotalPricePerDay,
                TotalHours = createContractBm.WorkHours,
                CotractName = createContractBm.JobTitle,

            };

            await this.db.Contracts.AddAsync(contract);

            await this.db.SaveChangesAsync();


            User customer = await ofcQueryByOfferIdSupplierId.Select(ocs => ocs.Customer).FirstOrDefaultAsync();

            User supplier = await ofcQueryByOfferIdSupplierId.Select(ocs => ocs.Supplier).FirstOrDefaultAsync();

            var ccs = await this.CreateContractCustomerSupplier(supplier, customer, contract);

            contract.ContractsCustomerSupplier = ccs;

            await this.db.SaveChangesAsync();

        }

        /// <summary>
        /// This method checks if supplier has already created a contract with this offerId and supplierId
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="offerId"></param>
        /// <returns>bool</returns>
        public async Task<bool> SupplierHasCreatedContract(string supplierId, int offerId)
        {
            return await this
                .db
                .OffersCustomerSupplier
                .AnyAsync(ocs => ocs.OfferId == offerId
                && ocs.SupplierId == supplierId
                && ocs.Offer.SupplierHasSentContract);

        }

        public async Task<bool> CustomerIsAuthortizedForThisContract(string cusutomerId, int contractId)
        {
            return await
                this.db
                .ContractsCustomersSupplier
                .AnyAsync(ccs => ccs.CustomerId == cusutomerId
                && ccs.ContractId == contractId);
        }

        public async Task ConfirmContract(string customerId, int contractId)
        {
            var query = this.db
                .ContractsCustomersSupplier
                .Where(ccs => ccs.CustomerId == customerId
                && ccs.ContractId == contractId).
                AsQueryable();

            var contract = await query.Select(c => c.Contract).FirstOrDefaultAsync();

            var job = await query.Select(c => c.Contract.Job).FirstOrDefaultAsync();

            contract.IsSignedByCustomer = true;

            job.IsJobTaken = true;

            await this.db.SaveChangesAsync();

        }

        public async Task<ConfrimContractVm> ContractDetails(string userId, int id)
        {
            return await this.db.ContractsCustomersSupplier
                .Where(ccs => ccs.CustomerId == userId && ccs.ContractId == id).ProjectTo<ConfrimContractVm>()
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// This method is called by CreateContractAsync() method and creates ContractsCustomerSupplier entity by given data
        /// and returns the creatred entity
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="customer"></param>
        /// <param name="contract"></param>
        /// <returns>ContractsCustomerSupplier</returns>
        private async Task<ContractsCustomerSupplier> CreateContractCustomerSupplier(User supplier, User customer, Contract contract)
        {
            ContractsCustomerSupplier ccs = new ContractsCustomerSupplier()
            {
                Contract = contract,
                Customer = customer,
                Supplier = supplier,
                SupplierId = supplier.Id,
                CustomerId = customer.Id,
                ContractId = contract.Id
            };
            await this.db.ContractsCustomersSupplier.AddAsync(ccs);

            await this.db.SaveChangesAsync();

            return ccs;
        }

    }
}
