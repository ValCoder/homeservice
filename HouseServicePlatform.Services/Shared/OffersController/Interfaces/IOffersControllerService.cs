﻿using HouseServicePlatform.Models.ViewModels.Customer.Contracts;

namespace HouseServicePlatform.Services.Shared.OffersController.Interfaces
{
    using Models.BindingModels.Supplier.Offers;
    using Models.DataModels;
    using Models.ViewModels.Customer.Offers;
    using Models.ViewModels.Supplier.Offers;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IOffersControllerService
    {
        Task CreateOfferAsync(string supplierId, User supplier, CreateOfferBm offerModel);

        Task<CreateOfferBm> GetCreateOfferVmAsync(int id);

        Task<ICollection<AllOffersSupplierVm>> GetAllOffersForSupplierAsync(string supplierId);

        Task<ICollection<AllOffersCustomerVm>> GetAllOffersForCustomerAsync(string customerId);

        Task<OfferCustomerDetails> GetCustomerOfferDetails(int offerId, string customerId);

        Task<bool> IsOfferAlreadySendedAsync(int id);

        Task<bool> CustomerCanReadOfferAsync(int offerId, string customerId);

        Task<bool> SupplierCanReadOfferAsync(int offerId, string supplierId);

        Task<ReadOffer> GetOfferToReadAsync(int id, string supplierId);

        Task<RequestContractVm> GetRequestContractVm(int offerId, string customerId);

        Task ConfirmRequestToSupplier(int offerId, string customerId);
    }
}
