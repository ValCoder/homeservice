﻿namespace HouseServicePlatform.Services.Shared.OffersController.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper.QueryableExtensions;
    using Data;
    using Models.BindingModels.Supplier.Offers;
    using Models.DataModels;
    using Models.ViewModels.Supplier.Offers;
    using Interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Models.ViewModels.Customer.Offers;
    using Models.ViewModels.Customer.Contracts;
    
    public class OffersControllerService : IOffersControllerService
    {
        private readonly HouseServiceDbContext db;
        private readonly UserManager<User> userManager;

        public OffersControllerService(HouseServiceDbContext db, UserManager<User> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        public async Task<CreateOfferBm> GetCreateOfferVmAsync(int id)
        {
            return await this.db
                  .JobServices
                  .Where(j => j.Id == id)
                  .ProjectTo<CreateOfferBm>()
                  .FirstOrDefaultAsync();
        }

        public async Task<ICollection<AllOffersSupplierVm>> GetAllOffersForSupplierAsync(string supplierId)
        {
            var supplierOffers = await this
                .db
                .OffersCustomerSupplier
                .Where(s => s.SupplierId == supplierId)
                .OrderByDescending(o => o.JobId)
                .ProjectTo<AllOffersSupplierVm>()
                .ToListAsync();

            return supplierOffers;
        }

        public async Task<ICollection<AllOffersCustomerVm>> GetAllOffersForCustomerAsync(string customerId)
        {
            var customerOffers = await this
                   .db
                   .OffersCustomerSupplier
                   .Where(c => c.CustomerId == customerId)
                   .OrderByDescending(o => o.JobId)
                   .ProjectTo<AllOffersCustomerVm>()
                   .ToListAsync();

            return customerOffers;
        }

        public async Task<OfferCustomerDetails> GetCustomerOfferDetails(int offerId, string customerId)
        {
            return await this.db
                .OffersCustomerSupplier
                .Where(ocs => ocs.OfferId == offerId && ocs.CustomerId == customerId)
                .ProjectTo<OfferCustomerDetails>()
                .FirstOrDefaultAsync();
        }

        public async Task<bool> IsOfferAlreadySendedAsync(int id)
        {
            return await this.db.OffersCustomerSupplier.AnyAsync(o => o.JobId == id && o.Offer.IsSent);
        }

        public async Task<bool> CustomerCanReadOfferAsync(int offerId, string customerId)
        {
            return await this.db
                   .OffersCustomerSupplier
                   .AnyAsync(ocs => ocs.OfferId == offerId && ocs.CustomerId == customerId);
        }

        public async Task<bool> SupplierCanReadOfferAsync(int offerId, string supplierId)
        {
            return await this.db
                .OffersCustomerSupplier.AnyAsync(ocs => ocs.OfferId == offerId && ocs.SupplierId == supplierId);
        }

        public async Task<ReadOffer> GetOfferToReadAsync(int id, string supplierId)
        {
            return await this
                .db
                .OffersCustomerSupplier
                .Where(ocs => ocs.SupplierId == supplierId && ocs.OfferId == id)
                .ProjectTo<ReadOffer>()
                .FirstOrDefaultAsync();
        }

        public async Task<RequestContractVm> GetRequestContractVm(int offerId, string customerId)
        {
            return await this
                .db
                .OffersCustomerSupplier
                .Where(ocs => ocs.OfferId == offerId && ocs.CustomerId == customerId && !ocs.Offer.CustomerHasRequestedContract)
                .ProjectTo<RequestContractVm>()
                .FirstOrDefaultAsync();
        }

        public async Task ConfirmRequestToSupplier(int offerId, string customerId)
        {
            var offerToConfirmRequest = await this
                 .db
                 .OffersCustomerSupplier
                 .Where(ocs =>
                     ocs.OfferId == offerId && ocs.CustomerId == customerId && !ocs.Offer.CustomerHasRequestedContract)
                     .Select(offer => offer.Offer)
                 .FirstOrDefaultAsync();

            if (offerToConfirmRequest != null)
            {
                offerToConfirmRequest.CustomerHasRequestedContract = true;

                await this.db.SaveChangesAsync();
            }

        }


        public async Task CreateOfferAsync(string supplierId, User supplier, CreateOfferBm offerModel)
        {
            var offer = new Offer()
            {
                Content = offerModel.Content,
                Title = offerModel.OfferTitle,
                IsSent = true
            };

            await this.db.Offers.AddAsync(offer);

            await this.db.SaveChangesAsync();

            await this.CreateOfferCustomerSupplier(supplierId, supplier, offerModel, offer);
        }

        private async Task CreateOfferCustomerSupplier(string supplierId, User supplier, CreateOfferBm offerBm, Offer offer)
        {
            var job = await this.db.JobServices.Where(j => j.Id == offerBm.jobId).FirstOrDefaultAsync();
            var customerId = job.CustomerId;
            var customer = await this.GetUserByIdAsync(customerId);

            var offerCustomerSupplier = new OffersCustomerSupplier()
            {
                Customer = customer,
                Job = job,
                JobId = offerBm.jobId,
                Supplier = supplier,
                SupplierId = supplierId,
                CustomerId = customerId,
                Offer = offer,
                OfferId = offer.Id
            };

            await this.db.OffersCustomerSupplier.AddAsync(offerCustomerSupplier);
            await this.db.SaveChangesAsync();
        }

        private async Task<User> GetUserByIdAsync(string userId)
        {
            return await this.userManager.FindByIdAsync(userId);
        }

    }
}
