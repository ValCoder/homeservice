﻿namespace HouseServicePlatform.Services.Supplier.HomeController.Implementations
{
    using AutoMapper.QueryableExtensions;
    using Data;
    using Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Models.BindingModels.Supplier.Jobs;
    using Models.ViewModels.Supplier.Jobs;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class HomeControllerService : IHomeControllerService
    {
        private readonly HouseServiceDbContext db;

        public HomeControllerService(HouseServiceDbContext db)
        {
            this.db = db;
        }


        public async Task<ICollection<AllJobsVmSupplier>> GetAllJobsAsync()
        {
            var viewModel = await this
                .db
                .JobServices
                .Where(j => !j.IsJobTaken)
                .OrderByDescending(j => j.Id)
                .ProjectTo<AllJobsVmSupplier>()
                .ToListAsync();

            return viewModel;
        }

        public async Task<ICollection<AllJobsVmSupplier>> FilterFobsAsync(FilteredJobSerach filterSearch)
        {

            // search only by filed of work
            if (filterSearch.City == null && filterSearch.Country == null)
            {
                return await this
                    .db
                    .JobServices
                    .Where(job => job.FieldOfWork == filterSearch.FieldOfWork)
                    .OrderByDescending(job => job.Id)
                    .ProjectTo<AllJobsVmSupplier>()
                    .ToListAsync();

            }

            return await this
                .db
                .JobServices
                .Where(job =>
                job.FieldOfWork == filterSearch.FieldOfWork
                && job.Location.City.Contains(filterSearch.City)
                || job.FieldOfWork == filterSearch.FieldOfWork
                && job.Location.Country.Contains(filterSearch.Country))
                .OrderByDescending(job => job.Id)
                .ProjectTo<AllJobsVmSupplier>()
                .ToListAsync();
        }

        public async Task<JobDetailsVm> GetJobDetailsAsync(int jobId)
        {
            return await this
                .db
                .JobServices
                .Where(j => j.Id == jobId)
                .ProjectTo<JobDetailsVm>()
                .FirstOrDefaultAsync();
        }

        public async Task<bool> JobExists(int jobId)
        {
            return await this.db.JobServices.AnyAsync(j => j.Id == jobId);
        }
    }
}
