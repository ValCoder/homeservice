﻿namespace HouseServicePlatform.Services.Supplier.HomeController.Interfaces
{
    using Models.BindingModels.Supplier.Jobs;
    using Models.ViewModels.Supplier.Jobs;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IHomeControllerService
    {
        Task<ICollection<AllJobsVmSupplier>> GetAllJobsAsync();

        Task<ICollection<AllJobsVmSupplier>> FilterFobsAsync(FilteredJobSerach filterSearch);

        Task<JobDetailsVm> GetJobDetailsAsync(int jobId);

        Task<bool> JobExists(int jobId);

    }
}
