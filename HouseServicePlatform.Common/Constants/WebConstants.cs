﻿namespace HouseServicePlatform.Common.Constants
{
    public static class WebConstants
    {
        public const string AdministratorRole = "Administrator";
        public const string SupplierRole = "SupplierRole";
        public const string CustomerRole = "CustomerRole";


        public const string AdminArea = "Admin";
        public const string SupplierArea = "Supplier";
        public const string CustomerArea = "Customer";

        public const string TemDataSuccessMesageKey = "SuccessMessage";
        public const string TemDataErrorMesageKey = "ErrorMessage";
        
    }
}
