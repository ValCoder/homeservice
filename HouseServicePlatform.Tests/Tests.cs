﻿namespace HouseServicePlatform.Tests
{
    using System;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models.DataModels;
    using Microsoft.AspNetCore.Identity;
    using Moq;
    using AutoMapper;
    using HouseServicePlatform.Web.Infrastructure.Mapping;

    public static class Tests
    {
        private static bool isInitialized;


        public static void InitializeAutoMapperProfile()
        {
            if (!isInitialized)
            {

                Mapper.Initialize(config => config.AddProfile<AutoMapperProfile>());

                isInitialized = true;
            }
        }

        public static Mock<UserManager<User>> GetUserManagerMock()
        {
            return new Mock<UserManager<User>>(Mock.Of<IUserStore<User>>(), null, null, null, null, null, null, null, null);
        }

        public static HouseServiceDbContext GetInMemoryDb()
        {
            var options = new DbContextOptionsBuilder<HouseServiceDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            return new HouseServiceDbContext(options);
        }

    }
}
