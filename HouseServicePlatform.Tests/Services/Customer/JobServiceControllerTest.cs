﻿namespace HouseServicePlatform.Tests.Services.Customer
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Data;
    using Models.BindingModels.Customers.Jobs;
    using Models.DataModels;
    using Models.Enums.SupplierCategories;
    using Models.ViewModels.Customer.Jobs;
    using HouseServicePlatform.Services.Customer.JobServiceController.Implementations;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Xunit;

    public class JobServiceControllerTest
    {
        private HouseServiceDbContext db;

        private const string CustomerId = "someId";
        private const string UnAutohrizedCustomerId = "SomeOtherCustomerId";


        public JobServiceControllerTest()
        {
            Tests.InitializeAutoMapperProfile();

            this.db = Tests.GetInMemoryDb();

        }

        [Fact]
        public async Task CreateJobServiceAsync_Should_Create_JobModel_In_Db()
        {
            // Arrange

            var userManagerMock = Tests.GetUserManagerMock();

            userManagerMock.Setup(u => u.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new User { Id = CustomerId });

            var jobServiceController = this.InitiateJobsServiceController();

            var jobModel = new CreateJobBm()
            {
                City = "",
                Country = "",
                Description = "",
                FieldOfWork = Category.Electrical,
                Neighborhood = "",
                PostalCode = "",
                StreetName = "",
                StreetNumber = 0,
                Title = ""
            };


            // Act

            var result = jobServiceController.CreateJobServiceAsync(jobModel, CustomerId);
            bool resultSavedInDb = await this.db.JobServices.AnyAsync(c => c.CustomerId == CustomerId);

            // Assert

            result.Should().NotBeNull();
            resultSavedInDb.Should().BeTrue();

        }

        [Fact]
        public async Task GetAllJobsAsync_Should_Return_List_Of_Type_AllJobsVmCustomer()
        {
            // Arrange  

            await this.PopulateDatabase();

            var jobServiceController = this.InitiateJobsServiceController();

            // Act 

            var results = await jobServiceController.GetAllJobsAsync(CustomerId);

            var query = this.db.JobServices.AsQueryable();

            List<bool> isResultDataEqualToDataInDb = new List<bool>();

            foreach (var result in results)
            {
                bool dataIsEqualToDataInDb = await query.AnyAsync(j => j.Id == result.Id);
                isResultDataEqualToDataInDb.Add(dataIsEqualToDataInDb);
            }

            var resultDataIsNotEqualToDataInDb = isResultDataEqualToDataInDb.Any(data => !data);

            // Assert

            results.Should().BeOfType<List<AllJobsVmCustomer>>();
            resultDataIsNotEqualToDataInDb.Should().BeFalse();
        }

        [Fact]
        public async Task GetAllJobsAsync_Should_Return_Empty_When_JobService_Db_Table_Is_Empty_With_Certain_CustomerId()
        {
            // Arrange 

            var jobServiceController = this.InitiateJobsServiceController();

            // Act

            var result = await jobServiceController.GetAllJobsAsync(CustomerId);

            // Assert

            result.Should().BeEmpty();
        }

        [Fact]
        public async Task EditAsync_Should_Return_JobService_From_DB()
        {
            // Arrange 

            await this.PopulateDatabase();

            int jobId = 1;


            var jobsService = this.InitiateJobsServiceController();

            // Act

            var result = await jobsService.EditAsync(jobId, CustomerId);

            bool dataIsEqualToDataInDb = await this.db.JobServices.AnyAsync(j => j.Id == result.Id);
            // Assert

            result.Should().NotBeNull();
            dataIsEqualToDataInDb.Should().BeTrue();
        }

        [Fact]
        public async Task CustomerIsAuthorOfJobAsync_Should_Return_True_With_Correct_Data()
        {
            // Arrange 
            await this.PopulateDatabase();
            var jobsService = this.InitiateJobsServiceController();
            int jobId = 2;

            // Act

            bool result = await jobsService.CustomerIsAuthorOfJobAsync(CustomerId, jobId);

            // Assert

            result.Should().BeTrue();
        }

        [Fact]
        public async Task CustomerIsAuthorOfJobAsync_Should_Return_False_With_InCorrect_Data()
        {
            // Arrange 
            await this.PopulateDatabase();
            var jobsService = this.InitiateJobsServiceController();
            int jobId = 2;

            // Act

            bool result = await jobsService.CustomerIsAuthorOfJobAsync(UnAutohrizedCustomerId, jobId);

            // Assert   

            result.Should().BeFalse();
        }

        [Fact]
        public async Task EditJobAsync_Should_Edit_Data_In_Db()
        {
            // Arrange 

            await this.PopulateDatabase();
            int jobId = 3;

            var jobService = InitiateJobsServiceController();

            EditJobBm editedJob = new EditJobBm()
            {
                City = "SomeCity",
                Country = "SomeCOuntrty",
                Description = "SomeDescription",
                Title = "EditedTitle",
                Neighborhood = "SomeOtherName",
                StreetName = "EditedStreetName",
                StreetNumber = 2,
                PostalCode = "222",
                Id = jobId
            };

            // Act 

            await jobService.EditJobAsync(editedJob, CustomerId);

            var job = await this.db.JobServices.FirstOrDefaultAsync(j => j.Id == editedJob.Id && j.CustomerId == CustomerId);

            bool jobIsEdited = job.Title == editedJob.Title
                               && job.Description == editedJob.Description
                               && job.Location.City == editedJob.City
                               && job.Location.Country == editedJob.Country
                               && job.Location.NeighborhoodName == editedJob.Neighborhood
                               && job.Location.StreetName == editedJob.StreetName
                               && job.Location.StreetNumber == editedJob.StreetNumber
                               && job.Location.PostalCode == editedJob.PostalCode;


            // Assert

            jobIsEdited.Should().BeTrue();
        }

        [Fact]
        public async Task DestroyJobAsync_With_Authorized_CustomerId_Should_Delete_Job_From_Db()
        {
            // Arrange

            await this.PopulateDatabase();

            var jobservice = this.InitiateJobsServiceController();

            int jobId = 2;



            // Act

            await jobservice.DestroyJobAsync(jobId, CustomerId);

            bool isJobDeleted = await this.db.JobServices.AnyAsync(j => j.Id != jobId);

            // Assert

            isJobDeleted.Should().BeTrue();
        }

        private JobsServiceController InitiateJobsServiceController()
        {
            return new JobsServiceController(this.db, Tests.GetUserManagerMock().Object);
        }

        #region Populate Database

        private async Task PopulateDatabase()
        {
            List<JobService> jobs = new List<JobService>()

                {
                new JobService()
                {
                    Id = 1,
                    IsJobTaken = false,
                    FieldOfWork = Category.FurnitureAssembly,
                    Title = "SomeTitle",
                    CustomerId = CustomerId,
                    Description = "SomeJobDescription",
                    Location = new Location()
                    {
                        City = "SomeCity",
                        Country = "SomeCountry",
                        Id = 2,
                        PostalCode = "213312321",
                        NeighborhoodName = "SomeName",
                        StreetNumber = 2,
                        UserId = CustomerId,
                        StreetName = "Name",

                    }

                },

                new JobService()
                {
                    Id = 2,
                    IsJobTaken = true,
                    FieldOfWork = Category.Gardner,
                    Title = "SomeTitle2",
                    CustomerId = CustomerId,
                    Description = "SomeJobDescription2",
                    Location = new Location()
                    {
                        City = "SomeCity",
                        Country = "SomeCountry",
                        Id = 2,
                        PostalCode = "213312321",
                        NeighborhoodName = "SomeName",
                        StreetNumber = 2,
                        UserId = CustomerId,
                        StreetName = "Name",

                    }

                },


                new JobService()
                {
                    Id = 3,
                    IsJobTaken = false,
                    FieldOfWork = Category.HouseKeeper,
                    Title = "SomeTitle3",
                    CustomerId = CustomerId,
                    Description = "SomeJobDescription3",
                    Location = new Location()
                    {
                        City = "SomeCity",
                        Country = "SomeCountry",
                        Id = 2,
                        PostalCode = "213312321",
                        NeighborhoodName = "SomeName",
                        StreetNumber = 2,
                        UserId = CustomerId,
                        StreetName = "Name",

                    }

                },

            };

            this.db.JobServices.AddRange(jobs);
            await this.db.SaveChangesAsync();

        }

        #endregion

    }
}
