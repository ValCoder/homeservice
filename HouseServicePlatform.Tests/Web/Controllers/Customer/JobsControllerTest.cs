﻿namespace HouseServicePlatform.Tests.Web.Controllers.Customer
{
    using Common.Constants;
    using FluentAssertions;
    using HouseServicePlatform.Services.Customer.JobServiceController.Interfaces;
    using HouseServicePlatform.Web.Areas.Customer.Controllers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models.BindingModels.Customers.Jobs;
    using Models.Enums.SupplierCategories;
    using Models.ViewModels.Customer.Jobs;
    using Moq;
    using NToastNotify;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;


    public class JobsControllerTest
    {

        [Fact]
        public void JobsController_Should_Be_Only_For_Authorized_Users_With_Role_Customer()
        {
            // Arrange 

            var controller = typeof(JobsController);

            // Act

            var controllerAttributes = controller.GetCustomAttributes(true);

            string authAtrrRoleName = string.Empty;

            foreach (var attr in controllerAttributes)
            {
                if (attr is AuthorizeAttribute authAtrr)
                {
                    authAtrrRoleName = authAtrr.Roles;
                }
            }

            // Assert 

            controllerAttributes.Should().Match(attr => attr.Any(a => a.GetType() == typeof(AuthorizeAttribute)));

            authAtrrRoleName.Should().Be(WebConstants.CustomerRole);

        }

        [Fact]
        public async Task All_GET_Should_Return_ViewResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            var jobsController = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act

            var result = await jobsController.All();

            // Assert

            controllerServiceMock.Verify(s => s.GetAllJobsAsync(It.IsAny<string>()), Times.Once);

            result
                .Should()
                .BeOfType<ViewResult>();

        }

        [Fact]
        public async Task CreateJob_GET_Should_Return_ViewResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act 

            var result = await controller.CreateJob();


            // Assert 

            controllerServiceMock.Verify(s => s.GetCreateJoViewModelAsync(It.IsAny<string>()), Times.Once());

            result
                .Should()
                .BeOfType<ViewResult>();

        }


        [Fact]
        public async Task CreateJob_POST_With_Valid_Model_State_Should_Return_RedirectToActionResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, Mock.Of<IToastNotification>());
            
            // Act 

            var controllerResult = await controller.CreateJob(It.IsAny<CreateJobBm>());

            // Assert 

            controllerServiceMock.Verify(s => s.CreateJobServiceAsync(It.IsAny<CreateJobBm>(), It.IsAny<string>()), Times.Once);

            controllerResult.Should().BeOfType<RedirectToActionResult>();

        }

        [Fact]
        public async Task CreateJob_POST_With_Invalid_Model_State_Should_Return_ViewResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act

            controller.ModelState.AddModelError("Invalid", "Invalid Model");

            var modelState = controller.ModelState;

            var controllerResult = await controller.CreateJob(It.IsAny<CreateJobBm>());

            // Assert

            modelState
                .IsValid.
                Should()
                .BeFalse();

            controllerResult
                .Should()
                .BeOfType<ViewResult>();

        }

        [Fact]
        public async Task Edit_GET_With_Authrozied_User_Should_Return_ViewResult()
        {
            // Arrange 
            int jobId = 2;

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(true);

            controllerServiceMock.Setup(s => s.EditAsync(jobId, It.IsAny<string>())).ReturnsAsync(Mock.Of<EditJobBm>());

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act

            var result = await controller.Edit(jobId);

            // Assert

            controllerServiceMock.Verify(s => s.EditAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);

            result
                .Should()
                .BeOfType<ViewResult>();

        }

        [Fact]
        public async Task Edit_GET_With_Unauthorized_User_For_Job_Should_Return_RedirectToAction_Result()
        {
            // Arrange 

            int jobId = 2;

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(false);

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, Mock.Of<IToastNotification>());

            // Act

            var result = await controller.Edit(jobId);

            // Assert

            controllerServiceMock.Verify(s => s.EditJobAsync(It.IsAny<EditJobBm>(), It.IsAny<string>()), Times.Never);

            result
                .Should()
                .BeOfType<RedirectToActionResult>();

        }

        [Fact]
        public async Task Edit_POST_With_Correct_Data_Should_Return_RedirectToAction()
        {
            // Arrange 
            
            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            var toastNotificationMock = new Mock<IToastNotification>();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(true);

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, toastNotificationMock.Object);

            // Act

            var result = await controller.Edit(Mock.Of<EditJobBm>());

            // Assert

            controllerServiceMock.Verify(s => s.EditJobAsync(It.IsAny<EditJobBm>(), It.IsAny<string>()), Times.Once);

            result
                .Should()
                .BeOfType<RedirectToActionResult>();

        }

        [Fact]
        public async Task Delete_GET_With_Authorized_User_Should_Return_ViewResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(true);

            controllerServiceMock.Setup(s => s.GetDeleteJobVmAsync(It.IsAny<int>(), It.IsAny<string>())).ReturnsAsync(Mock.Of<DeleteJobVm>());

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act

            var result = await controller.Delete(It.IsAny<int>());

            // Assert

            controllerServiceMock.Verify(s => s.GetDeleteJobVmAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);

            result
                .Should()
                .BeOfType<ViewResult>();
        }

        [Fact]
        public async Task Delete_GET_With_Unauthorized_User_Should_Return_RedirectToActionResult()
        {
            // Arrange 

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(false);
            
            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, Mock.Of<IToastNotification>());

            // Act

            var result = await controller.Delete(It.IsAny<int>());

            // Assert

            controllerServiceMock.Verify(s=> s.GetDeleteJobVmAsync(It.IsAny<int>(),It.IsAny<string>()), Times.Never);

            result
                .Should()
                .BeOfType<RedirectToActionResult>();
        }

        [Fact]
        public async Task Destroy_With_Authorized_User_Should_Return_RedirectToActionResult()
        {
            // Arrange

            int jobId = 2;

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();

            controllerServiceMock.Setup(s => s.CustomerIsAuthorOfJobAsync(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(true);

            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, Mock.Of<IToastNotification>());

            // Act

            var result = await controller.Destroy(jobId);

            // Assert


            controllerServiceMock.Verify(s => s.DestroyJobAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);

            result
                .Should()
                .BeOfType<RedirectToActionResult>();
        }

        [Fact]
        public async Task Destroy_With_Invalid_JobId_Should_Return_BadRequestResult()
        {
            // Arrange

            int jobId = 0;

            var controllerServiceMock = new Mock<IJobsServiceController>();

            var userManagerMock = Tests.GetUserManagerMock();
            
            var controller = new JobsController(controllerServiceMock.Object, userManagerMock.Object, null);

            // Act

            var result = await controller.Destroy(jobId);

            // Assert


            controllerServiceMock.Verify(s => s.DestroyJobAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Never);

            result
                .Should()
                .BeOfType<BadRequestResult>();
        }

      
        
        //private EditJobBm GetEditJobBindModel()
        //{
        //    return new EditJobBm()
        //    {
        //        FieldOfWork = Category.Electrical,
        //        Title = "SomeTitle",
        //        City = "SomeCity",
        //        StreetNumber = 1,
        //        Description = "SomeDescription",
        //        PostalCode = "SomePostalCode",
        //        StreetName = "SomeStreeName",
        //        Country = "TestCpuntryName",
        //        Neighborhood = "TestNeighborhood",
        //        Id = 1
        //    };
        //}

   

    }
}
